 # Makefile for shifter

PDLIBBUILDER_DIR=.
DOXYGEN=/Applications/Doxygen.app/Contents/Resources/doxygen

lib.name = shifter

class.sources = shifter~.c

datafiles = shifter-help.pd

include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder


#-------Documentation-----
doc:
	${DOXYGEN} Doxyfile.cnf
	astyle --style=java --suffix=none --errors-to-stdout *.c 
